<html>
	<head>
	  	<meta content='text/html; charset=UTF-8' http-equiv="Content-Type" />
	
	  	<link href="assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	  	<link href="assets/css/style.css" rel="stylesheet">
	  	
	  	<script src="assets/libs/jquery/2.1.1/jquery.min.js"></script>	  
	  	<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
	  	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	  	<script src="assets/libs/angularjs/angular.min.js"></script>
	  	<script src="assets/libs/angularjs/angular-cookies.min.js"></script>
	  	<script src="assets/libs/angularjs/i18n/angular-locale_pt-br.js"></script>
		<script src="assets/libs/angularjs/angular-sanitize.min.js"></script>
	  
	</head>
	<body>
		<form method="POST" action="login">
			<div class="row">
				<div class="col-12">
					<div class="formCadastro">
						<div class="formCadastro-header">
							<label>Login</label>
						</div>
						<div class="formCampos">
						  <div class="form-group">
						    <label>Login</label>
						    <input class="form-control" name="username">
						  </div>
						  <div class="form-group">
						    <label>Senha</label>
						    <input type="password" class="form-control" name="password">
						  </div>
						<div>
							<a href="novo-usuario.jsp">Cadastre-se</a>
						</div>
					  </div>
					  <div class="formCadastro-footer">
					  	<button type="submit" class="btn btn-primary btnSubmit">Entrar</button>
					  </div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>