<html ng-app="app">
	<head>
	  	<meta content='text/html; charset=UTF-8' http-equiv="Content-Type" />
		
	  	<link href="assets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	  	<link href="assets/libs/ui-bootstrap/ui-bootstrap.css" rel="stylesheet">
		
		<!-- Latest compiled and minified JavaScript -->
	  	
	  	<link href="assets/libs/angular-growl/css/growl.css" rel="stylesheet">
	  	<link href="assets/css/style.css" rel="stylesheet">
	  	
<!-- 	  	<script src="assets/libs/jquery/2.1.1/jquery.min.js"></script>	   -->
<!-- 		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
	  	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	  	<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
	  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
	  	<script src="assets/libs/angularjs/angular.min.js"></script>
	  	<script src="assets/libs/angularjs/angular-cookies.min.js"></script>
	  	<script src="assets/libs/angularjs/i18n/angular-locale_pt-br.js"></script>
		<script src="assets/libs/angularjs/angular-sanitize.min.js"></script>
		<script src="assets/libs/angularjs/angular-animate.min.js"></script>
		<script src="assets/libs/angular-growl/js/growl.js"></script>
		<script src="assets/libs/ui-bootstrap/ui-bootstrap.min.js"></script>
	  
	</head>
	<body>
	<a class="float-right" href="logout">Oi, ${usuario.nome} - Logout!</a><br>
		<div class="row">
			<div class="col-12">
				<nav class="navbar navbar-expand-lg navbar-light navbar-menu">
				    <ul class="navbar-nav navbar-custom">
				      <li class="nav-item active">
				        <a class="nav-link" href="#!/time">Time</a>
				      </li>
				    </ul>
				</nav>
			</div>
		</div>
		<div class="form-center">
			<div class="row">
				<div class="col-12">
					<div ng-view class="container">
					</div>
				</div>
			</div>
		</div>
		
		<script src="assets/libs/angularjs/angular-resource.min.js"></script>
  		<script src="assets/libs/angularjs/angular-route.min.js"></script>
		<script src="app/app.module.js"></script>
  		<script src="app/app.config.js"></script>
  		<script src="app/time/time-service.js"></script>
  		<script src="app/time/time-controller.js"></script>
	</body>
</html>