(function() {
	'use strict';
	angular.module('app', 
			['ngResource',
			 'ngRoute',
			 'ui.growl',
			 'ui.bootstrap',
			 // app modules
			 'app.time'
			 ]);
})();