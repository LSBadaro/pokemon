<div class="dtTimeIndex">
	<div class="row" >
		<div class="col-12">
			<div class="conteudo">
				<div class="btn-group btnGroup" role="group" aria-label="Basic example">
				  <button type="button" class="btn btn-outline-success" ng-click="newTime()">Novo</button>
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-condensed">
					  <thead class="thead-dark"">
					    <tr>
					      <th class="text-center" scope="col">#</th>
					      <th scope="col">Nome do Time</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr ng-repeat="time in vm.times" ng-click="edit(time.id)">
					      <th class="text-center" scope="row">{{time.id}}</th>
					      <td>{{time.nome}}</td>
					    </tr>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
