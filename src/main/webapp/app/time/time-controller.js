(function() {
	'use strict';
	angular.module('app.time', ['app', 'app.time.service'])
			.controller('TimeIndexCtrl', TimeIndexCtrl)
			.controller('TimeEditCtrl', TimeEditCtrl);

	/**
	 * Index.
	 */
	function TimeIndexCtrl( $scope, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, TimeService) {

		var vmscope = $scope;
		var vm = this;
		vm.times = [];
		
		$scope.totalItems = 64;
		  $scope.currentPage = 4;

		  $scope.setPage = function (pageNo) {
		    $scope.currentPage = pageNo;
		  };

		  $scope.maxSize = 5;
		  $scope.bigTotalItems = 175;
		  $scope.bigCurrentPage = 1;
		
		vmscope.newTime = function() {
			$window.location.href = '#!/time/0';
		}

		vmscope.edit = function(id) {
			$window.location.href = '#!/time/' + id;
		}
		
		vm.listarTimes = function() {
			TimeService.time().then(function(resp) {
				  vm.times = resp.data;
			});
		}
		vm.listarTimes();
	}
	
	function TimeEditCtrl($scope, $resource, $timeout, $route,
			$routeParams, $http, $location, $q, $window, $growl, TimeService) {

		var vmscope = $scope;
		var vm = this;

		vm.pokemons = [];
		vm.pokemonsTime = [];
		vm.habilidades = [];
		vm.pokemonsMovesTime = [];
		vm.hasPokemon = false;
		vmscope.newTitle = "Time";
		
		vmscope.entity = {
				id: null,
				nome:null,
				pokemonTime: []
		}
		
		vmscope.save = function() {
			if(vmscope.entity.nome != null) {
				TimeService.save(vmscope.entity).then(function(resp) {
					vmscope.entity = resp.data;
					if(resp.status == 200) {
						$growl.box('Time', 'Dados salvos com sucesso!', {
				            class: 'success',
				            sticky: false,
				            timeout: 5000
				        }).open();
					}
				});
			} else {
				$growl.box('Time', 'Preencha o campo!', {
		            class: 'warning',
		            sticky: false,
		            timeout: 5000
		        }).open();
			}
		}
		
		vmscope.edit = function(pokemon) {
			vm.hasPokemon = true;
			vmscope.nomePokemon = pokemon.nomePokemon;
			vm.pokemonMoves = pokemon;
		}
		
		vmscope.back = function() {
			$window.location.href = '#!/time';
		}
		
		vm.listarHabilidades = function() {
			TimeService.habilidades().then(function(resp) {
				  vm.habilidades = resp.data;
			});
		}
		
		vm.carregarEntidade = function() {
			if($routeParams.timeId > 0) {
				TimeService.carregarEntidade($routeParams.timeId).then(function(resp){
					vmscope.entity = resp.data;
				});
			}
		}
		vm.carregarEntidade();
		
		
		vm.listarPokemons = function() {
			TimeService.pokemons().then(function(resp) {
				  vm.pokemons = resp.data;
			});
		}
		
		vm.addNovoPokemon = function(pokemon, idx) {
			vm.pokemon = {
					idPokemon: null,
					nomePokemon: null
			};
			if(vm.pokemonsTime.length < 6 && pokemon.selected == true) {
				if(pokemon != null) {
					if(pokemon.selected == true) {
						vm.pokemon.idPokemon = idx;
						vm.pokemon.nomePokemon = pokemon.name;
						vm.pokemonsTime.push(vm.pokemon);
					} else {
						vm.idx = vm.pokemonsTime.indexOf(pokemon);
						vm.pokemonsTime.splice(vm.idx, 1); 
					}
				}
			} else {
				$growl.box('Time', 'Quantidade de pokemons permitidade: 6 !', {
		            class: 'warning',
		            sticky: false,
		            timeout: 5000
		        }).open();
			}
		}
		
		vm.addNovaHabilidade = function(habilidade, idx) {
			vm.habilidade = {
					idMove: null,
					nomeMove: null
			};
			
			if(habilidade != null) {
				if(habilidade.selected == true) {
					vm.habilidade.idMove = idx;
					vm.habilidade.nomeMove = habilidade.name;
					vm.pokemonsMovesTime.push(vm.habilidade);
				} else {
					vm.idxHabilidade = vm.pokemonsMovesTime.indexOf(habilidade);
					vm.pokemonsMovesTime.splice(vm.idxHabilidade, 1); 
				}
			}
		}
		
		vm.addHabilidade = function() {
			if(vm.pokemonsMovesTime != null) {
				if(vm.pokemonMoves.pokemonMovesTime == null) {
					vm.pokemonMoves.pokemonMovesTime = [];
				}
				vm.pokemonMoves.pokemonMovesTime = vm.pokemonMoves.pokemonMovesTime.concat(vm.pokemonsMovesTime);
			}
			vm.pokemonsMovesTime = [];
		}
		
		vm.addPokemon = function() {
			if(vm.pokemonsTime != null) {
				if(vmscope.entity.pokemonTime == null){
					vmscope.entity.pokemonTime = [];
				}
				vmscope.entity.pokemonTime = vmscope.entity.pokemonTime.concat(vm.pokemonsTime);
			}
			vm.pokemonsTime = [];
		}
	}

})();