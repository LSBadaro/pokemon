<div class="dtTimeEdit">
	<div class="row" >
		<div class="col-12">
			<div class="conteudo">
				<div class="row">
					<div class="col-sm-12 conteudo-header">
						<span style="font-size: 30px">{{newTitle}}</span>
						<div class="btn-group btnGroup" role="group" aria-label="Basic example">
					  		<button type="button" class="btn btn-outline-success" ng-click="save()">Salvar</button>
						  	<button type="button" class="btn btn-outline-secondary" ng-click="back()">Voltar</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-success alert-dismissible fade show" role="alert" ng-model="vm.messsage" ng-show="vm.hasMessage">
						  {{message}}
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>
					</div>
				</div>
				<form class="conteudo-body">
				  <div class="form-row">
				    <div class="col-md-3 mb-3">
				      <label for="validationCustom01">Nome:</label>
				      <input type="text" class="form-control" id="validationCustom01" placeholder="Nome do seu time..." ng-model="entity.nome" required>
				    </div>
				    <div class="col-md-3">
				    	<div class="btnAdicionar">
						    <button type="button" class="btn btn-outline-primary float-right" ng-click="vm.listarPokemons()" data-toggle="modal" data-target="#modalPokemons">
						  	Adicionar Pokemons
							</button>
						</div>
				    </div>
				  </div>
				</form>
				<div class="row">
					<div class="col-sm-6">
						<div class="table-responsive">
							<table class="table table-striped table-condensed table-bordered">
							  <thead class="thead-dark"">
							    <tr>
							      <th class="text-center" scope="col">#</th>
							      <th scope="col">Nome do Pokemon</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr ng-repeat="pokemon in entity.pokemonTime" ng-click="edit(pokemon)">
							      <th class="text-center" scope="row">{{pokemon.idPokemon}}</th>
							      <td style="text-transform: capitalize">{{pokemon.nomePokemon}}</td>
							    </tr>
							  </tbody>
							</table>
						</div>
					</div>
					<div ng-show="vm.hasPokemon" class="col-sm-6 habilidades">
						<div class="row" style="margin-bottom: 30px; margin-top: -33px;">
							<div class="col-md-6">
								<h4>Habilidades <span style="text-transform: capitalize">{{nomePokemon}}</span></h4>
							</div>
							<div class="col-md-6">
						    	<div class="">
								    <button type="button" class="btn btn-outline-primary float-right" ng-click="vm.listarHabilidades()" data-toggle="modal" data-target="#modalHabilidades">
								  	Adicionar habilidades
									</button>
								</div>
						    </div>
					    </div>
						<div class="table-responsive">
							<table class="table table-striped table-habilidades">
							  <thead class="thead-dark"">
							    <tr>
							      <th class="text-center" scope="col">#</th>
							      <th scope="col">Habilidade</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr ng-repeat="item in vm.pokemonMoves.pokemonMovesTime">
							      <th class="text-center" scope="row">{{item.idMove}}</th>
							      <td style="text-transform: capitalize">{{item.nomeMove}}</td>
							    </tr>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Pokemon-->
<div class="modal fade" id="modalPokemons" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pokemons</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
			<table class="table table-striped table-condensed table-bordered">
			  <thead class="thead-dark"">
			    <tr>
			      <th class="text-center" scope="col">#</th>
			      <th scope="col">Nome do Time</th>
			      <th></th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr ng-repeat="pokemon in vm.pokemons.results">
			      <th class="text-center" scope="row">{{$index+1}}</th>
			      <td>{{pokemon.name}}</td>
			      <td><input type="checkbox" ng-show="entity.pokemonsTime.length < 6 || entity.pokemonsTime == null" ng-model="pokemon.selected"
			       ng-change="vm.addNovoPokemon(pokemon, $index+1)"></td>
			    </tr>
			  </tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="vm.addPokemon()" data-dismiss="modal">Adicionar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Habilidades-->
<div class="modal fade" id="modalHabilidades" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Habilidades</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
			<table class="table table-striped table-condensed table-bordered">
			  <thead class="thead-dark"">
			    <tr>
			      <th class="text-center" scope="col">#</th>
			      <th scope="col">Habilidade</th>
			      <th></th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr ng-repeat="item in vm.habilidades.results">
			      <th class="text-center" scope="row">{{$index+1}}</th>
			      <td>{{item.name}}</td>
			      <td><input type="checkbox" ng-model="item.selected"
			       ng-change="vm.addNovaHabilidade(item, $index+1)"></td>
			    </tr>
			  </tbody>
			</table>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" ng-click="vm.addHabilidade()" data-dismiss="modal">Adicionar</button>
      </div>
    </div>
  </div>
</div>