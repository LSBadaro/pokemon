(function(){
	'use strict';
	angular.module('app.time.service', 
			['app']).factory('TimeService', TimeService);
	
	TimeService.$inject = ['$http', '$resource'];
	function TimeService($http, $resource) {
		return {
			time: function(){
				return $http.get('rest/times');
			},
			pokemons: function(){
				return $http.get('rest/times/pokemons');
			},
			save: function(time){
				return $http.post('rest/times', time);
			},
			carregarEntidade: function(id) {
				return $http.get('rest/times/entidade/' + id);
			},
			habilidades: function(){
				return $http.get('rest/times/habilidades');
			}

		};
	}
	
})();