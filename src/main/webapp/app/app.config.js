(function() {
	'use strict';
angular.
  module('app').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/time', {templateUrl: 'app/time/time-index.jsp',
        	controller: 'TimeIndexCtrl', controllerAs: 'vm'
        }).
        when('/time/:timeId', {templateUrl: 'app/time/time-edit.jsp',
        	controller: 'TimeEditCtrl', controllerAs: 'vm'
        })
        .otherwise('/time');
    }
  ]);
})();