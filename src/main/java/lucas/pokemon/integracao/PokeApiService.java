package lucas.pokemon.integracao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import lucas.pokemon.time.Time;
import lucas.pokemon.usuario.Usuario;

@Stateless
public class PokeApiService {

	@PersistenceContext
	private EntityManager em;

	public List<Time> recuperarTimes(Long idUsuario, String nome, int offset, int limit) {
		return em.createNamedQuery("procurarTime", Time.class).setParameter("idUsuario", idUsuario)
				.setFirstResult(offset).setMaxResults(limit).getResultList();
	}

	public String recuperarPokemon(int offset, int limit) {
		String json = null;
		Client client = ClientBuilder.newClient();
		try {
			json = client.target("https://pokeapi.co/api/v2/pokemon/").queryParam("limit", 15)
					.request(MediaType.APPLICATION_JSON).get(String.class);
		} catch (Exception e) {
			e.getMessage();
		}

		return json;
	}

	public String recuperarHabilidades(int offset, int limit) {
		String json = null;
		Client client = ClientBuilder.newClient();
		try {
			json = client.target("https://pokeapi.co/api/v2/move/").request(MediaType.APPLICATION_JSON)
					.get(String.class);
		} catch (Exception e) {
			e.getMessage();
		}

		return json;
	}

	public Time salvarTime(Time time) {
		verificarPokemonTime(time);
		em.persist(time);
		return time;
	}

	private void verificarPokemonTime(Time time) {
		if (time.getPokemonTime() != null) {
			time.getPokemonTime().forEach(item -> {
				if (item.getTime() == null) {
					item.setTime(time);
				}
				if (item.getPokemonMovesTime() != null) {
					item.getPokemonMovesTime().forEach(move -> {
						if (move.getPokemonTime() == null) {
							move.setPokemonTime(item);
						}
					});
				}
			});
		}
	}

	public Time recuperarTime(Long id) {
		return em.createNamedQuery("recuperarEntidade", Time.class).setParameter("id", id).getSingleResult();
	}

	public Time alterarTime(Time time) {
		verificarPokemonTime(time);
		em.merge(time);
		return time;
	}

	public void criarUsuario(Usuario usuario) {
		em.persist(usuario);
	}

	public Usuario login(String login, String senha) {
		
		em.createQuery("select usuario from Usuario usuario ", Usuario.class).getResultList();		
		
		List<Usuario> result = em.createQuery(
				"select usuario from Usuario usuario where usuario.login = :login and usuario.senha = :senha",
				Usuario.class).setParameter("login", login).setParameter("senha", senha).getResultList();
		
		if (result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}
}
