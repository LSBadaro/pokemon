package lucas.pokemon.pokemon;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lucas.pokemon.time.Time;

@Entity
public class PokemonTime {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	private Long idPokemon;
	
	@NotNull
	private String nomePokemon;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.DETACH })
	@JoinColumn(name = "idTime", nullable=false)
	private Time time;
	
	@OneToMany(mappedBy = "pokemonTime", fetch = FetchType.LAZY, orphanRemoval = true ,cascade = CascadeType.ALL)
	private Set<PokemonMovesTimes> pokemonMovesTime;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPokemon() {
		return idPokemon;
	}

	public void setIdPokemon(Long idPokemon) {
		this.idPokemon = idPokemon;
	}

	public String getNomePokemon() {
		return nomePokemon;
	}

	public void setNomePokemon(String nomePokemon) {
		this.nomePokemon = nomePokemon;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Set<PokemonMovesTimes> getPokemonMovesTime() {
		return pokemonMovesTime;
	}

	public void setPokemonMovesTime(Set<PokemonMovesTimes> pokemonMovesTime) {
		this.pokemonMovesTime = pokemonMovesTime;
	}
	
}
