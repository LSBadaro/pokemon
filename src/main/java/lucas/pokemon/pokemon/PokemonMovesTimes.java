package lucas.pokemon.pokemon;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class PokemonMovesTimes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	private Long idMove;

	@NotNull
	private String nomeMove;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.DETACH })
	@JoinColumn(name = "idPokemonTime", nullable=false)
	private PokemonTime pokemonTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdMove() {
		return idMove;
	}

	public void setIdMove(Long idMove) {
		this.idMove = idMove;
	}

	public String getNomeMove() {
		return nomeMove;
	}

	public void setNomeMove(String nomeMove) {
		this.nomeMove = nomeMove;
	}

	public PokemonTime getPokemonTime() {
		return pokemonTime;
	}

	public void setPokemonTime(PokemonTime pokemonTime) {
		this.pokemonTime = pokemonTime;
	}

}
