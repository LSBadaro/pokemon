package lucas.pokemon;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lucas.pokemon.integracao.PokeApiService;
import lucas.pokemon.usuario.Usuario;

@WebServlet(urlPatterns="/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private PokeApiService svc;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String login = req.getParameter("username");
		String senha = req.getParameter("password");
		
		Usuario usuario = svc.login(login, senha);
		if (usuario != null) {
			req.getSession(true).setAttribute("usuario", usuario);
			resp.sendRedirect(req.getContextPath());
		} else {
			resp.sendRedirect(req.getContextPath() + "/login-erro.jsp");
		}
	}

}
