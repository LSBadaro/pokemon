package lucas.pokemon.rest;

import java.net.URI;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lucas.pokemon.integracao.PokeApiService;
import lucas.pokemon.usuario.Usuario;

@Path("/usuarios")
public class UsuarioResource {

	@EJB
	private PokeApiService svc;
	
	@Context
	private HttpServletResponse response;
		
	@Context
	private HttpServletRequest request;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response cadastrarUsuario(@FormParam("nome") String nome, @FormParam("login") String login, @FormParam("senha") String senha) {
		Usuario usuario = new Usuario();
		usuario.setNome(nome);
		usuario.setLogin(login);
		usuario.setSenha(senha);
		svc.criarUsuario(usuario);
		
		return Response.temporaryRedirect(URI.create("../")).build();
	}
}
