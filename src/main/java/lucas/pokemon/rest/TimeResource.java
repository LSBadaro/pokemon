package lucas.pokemon.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lucas.pokemon.integracao.PokeApiService;
import lucas.pokemon.pokemon.PokemonMovesTimes;
import lucas.pokemon.pokemon.PokemonTime;
import lucas.pokemon.time.Time;
import lucas.pokemon.usuario.Usuario;

@Path("/times")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TimeResource {
	
	@EJB
	private PokeApiService svc;
	
	@Context
	private HttpServletRequest request;
	
	@GET
	public Response searchTimes(@QueryParam("nome") final String nome,
						  @QueryParam("page") @DefaultValue("1") final int page,
                          @QueryParam("limit") @DefaultValue("20") final int limit) throws Exception {
		
//		svc.recuperarPokemon();
		List<Time> times = new ArrayList<>();
		int offset = page * limit - limit;
		try {
			Usuario usuario = getUsuario();
			times = svc.recuperarTimes(usuario.getId(), nome, offset, limit);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return Response.ok(times).build();
	}
	
	private Usuario getUsuario() {
		return (Usuario) request.getSession().getAttribute("usuario");
	}
	
	@GET
	@Path("/pokemons")
	public Response searchPokemons(@QueryParam("page") @DefaultValue("1") final int page,
        						   @QueryParam("limit") @DefaultValue("20") final int limit) throws Exception { 
		
		final String pokemons;
		int offset = page * limit - limit;
		try {
			pokemons = svc.recuperarPokemon(offset, limit);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return Response.ok(pokemons).build();
		
	}
	
	@GET
	@Path("/entidade/{id}")
	public Time recuperarTime(@PathParam("id") final Long id) {
		if(id != null) {
			Time time = svc.recuperarTime(id);
			prepararParaJson(time);
			return time;
		}
		return null;
	}
	
	@GET
	@Path("/habilidades")
	public Response searchMoves(@QueryParam("page") @DefaultValue("1") final int page,
				    			@QueryParam("limit") @DefaultValue("20") final int limit) throws Exception {
		
		final String habilidades;
		int offset = page * limit - limit;
		try {
			habilidades = svc.recuperarHabilidades(offset, limit);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return Response.ok(habilidades).build();
	}
	
	@POST
	public Time save(Time time) {
		if(time != null && time.getId() == null) {
			Usuario usuario = getUsuario();
			time.setIdUsuario(usuario.getId());
			svc.salvarTime(time);
		} else if(time.getId() > 0) {
			svc.alterarTime(time);
		}
		prepararParaJson(time);
		return time;
	}
	
	private void prepararParaJson(final Time entity) {
        if (entity.getPokemonTime() != null) {
            for (final PokemonTime i : entity.getPokemonTime()) {
                i.setTime(null);
                if(i.getPokemonMovesTime() != null) {
	                for(final PokemonMovesTimes p : i.getPokemonMovesTime()) {
	                	p.setPokemonTime(null);
	                }
                }
            }
        }
    }
}
