package lucas.pokemon.time;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import lucas.pokemon.pokemon.PokemonTime;
import lucas.pokemon.usuario.Usuario;

@Entity

@NamedQueries({
	@NamedQuery(name="procurarTime",
		query="select new Time(time.id, time.nome) "
		+ "from Time time "
		+ "where time.idUsuario = :idUsuario"),
	@NamedQuery(name="recuperarEntidade",
		query="select time"
		+" from Time time "
		+ "left join fetch time.pokemonTime pokemon "
		+ "left join fetch pokemon.pokemonMovesTime "
		+ "where time.id = :id")})
public class Time {
	
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	private String nome;

    @Column(name = "idUsuario", nullable=false)
    private Long idUsuario;
	
	@OneToMany(mappedBy = "time", fetch = FetchType.LAZY, orphanRemoval = true ,cascade = CascadeType.ALL)
	private Set<PokemonTime> pokemonTime;
	
	public Time() {
		//noop
	}
	
	public Time(
			final Long id,
			final String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Set<PokemonTime> getPokemonTime() {
		return pokemonTime;
	}

	public void setPokemonTime(Set<PokemonTime> pokemonTime) {
		this.pokemonTime = pokemonTime;
	}
 	
}
